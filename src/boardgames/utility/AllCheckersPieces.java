package boardgames.utility;

/**
 * this enumeration defines the type of all checkers pieces.
 */
public enum AllCheckersPieces {

    /**
     * checkers pieces.
     */
    Pawn, King;
}
