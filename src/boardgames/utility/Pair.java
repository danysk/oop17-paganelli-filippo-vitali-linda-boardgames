package boardgames.utility;

/**
 * This interface defines the functions useful for modeling a 2D point.
 * @param <X> the x coordinate of the Pair
 * @param <Y> the y coordinate of the Pair
 */
public interface Pair<X, Y> {

    /**
     * @return the X component of the Pair
     */
    X getX();

    /**
     * @return the Y component of the Pair
     */
    Y getY();

    /**
     * @return an integer representing an hash code value for the object.
     */
    int hashCode();

    /**
     * @param obj the Object that has to be compared with this Pair
     * @return true if obj is equals to this Pair, otherwise false
     */
    boolean equals(Object obj);

    /**
     * @return the String representation of this Pair
     */
    String toString();
}
