package boardgames.model.piece;

import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.Colour;

/**
 * This interface define the functions useful to modeling new type of piece.
 */
public interface PieceType {

    /**
     * @param b Board on which you are playing
     * @param current a Box that is the current position of the Piece
     * @param colour Colour of the Piece
     * @return all the possible moves that the Piece can do
     */
    List<Box> possibleMoves(BoardImpl b, Box current, Colour colour);

    /**
     * @return the String representation of the piece's name
     */
    String getName();
}
