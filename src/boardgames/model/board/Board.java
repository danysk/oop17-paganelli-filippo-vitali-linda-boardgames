package boardgames.model.board;

import java.util.List;
import java.util.Optional;

import boardgames.model.piece.PieceImpl;

/**
 * This class defines the functions useful for modeling the common Board.
 */
public interface Board {

    /** 
     * Set the type of Board.
     * @param type type of Board
     */
    void setBoardType(BoardType type);

    /**
     * @return an optional of BoardType representing the type of board, otherwise an empty optional
     */
    Optional<BoardType> getBoardType();

    /**
     * Initializes the Board.
     */
    void reset();

    /**
     * @param x integer indicating the x coordinate
     * @param y integer indicating the y coordinate
     * @return an optional representing the Box having x and y coordinates if it's present in the board
     * @throws IllegalArgumentException if the indicated coordinates are out of the Board
     */
    Optional<Box> getBox(int x, int y);

    /**
     * @return a List of List of Box containing all the Board's Boxes
     */
    List<List<Box>> getAllBoxes();

    /**
     * @return a List containing white's pieces
     */
    List<PieceImpl> getWhitePieces();

    /**
     * @param pw Piece to add to the white pieces
     * @throws IllegalArgumentException if the Piece indicated has a different Colour from the Player
     */
    void addWhitePiece(PieceImpl pw);

    /**
     * @param pb the white piece to be removed from the list of white pieces
     */
    void removeWhitePiece(PieceImpl pb);

    /**
     * @return a List containing black's pieces
     */
    List<PieceImpl> getBlackPieces();

    /**
     * @param pb Piece to add to the black pieces
     * @throws IllegalArgumentException if the Piece indicated has a different Colour from the Player
     */
    void addBlackPiece(PieceImpl pb);

    /**
     * @param pb the black piece to be removed from the list of black pieces
     */
    void removeBlackPiece(PieceImpl pb);
}
