package boardgames.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;
import boardgames.view.game.GameView;

/**
 * This abstract class implements the common game model and defines 
 * the abstract functions useful for specifying new game logics.
 */
public abstract class GameImpl implements Game {

    /**
     * Indicates the index of the upper row of the board.
     */
    protected static final int UPPER_BOUND = 7;
    /**
     * Indicates the index of the lower row of the board.
     */
    protected static final int LOWER_BOUND = 0;
    private final BoardImpl board;
    private Player whitePlayer;
    private Player blackPlayer;
    private final List<GameView> observers;
    private final List<PieceImpl> lastPieceEated;
    private Optional<PieceImpl> lastPieceMoved;
    private static final Logger LOGGER = LogManager.getLogger(GameImpl.class.getName());

    /**
     * 
     */
    public GameImpl() {
        this.lastPieceEated = new LinkedList<>();
        this.observers = new LinkedList<>();
        this.board = new BoardImpl();
        this.lastPieceMoved = Optional.empty();
    }

    @Override
    public final BoardImpl getBoard() {
        return this.board;
    }

    @Override
    public final boolean move(final Colour player, final PieceImpl pieceToMove, final Optional<Box> newPosition) {
        this.lastPieceEated.clear();
        //controllo se il giocatore possiede il pezzo passato
        if (this.getPlayer(player).getPlayerPieces().contains(pieceToMove)) {
                //controllo se il pezzo può spostarsi nella nuova posizione indicata
            if (!newPosition.isPresent()) {
                LOGGER.debug("empty optional passed as Box in the call to move");
                throw new IllegalArgumentException();
            }
            if (this.checkMove(newPosition.get(), pieceToMove)) {
                //se è presente un pezzo avversario lo mangia e si sposta
                if (this.checkEat(newPosition.get(), pieceToMove)) {
                    this.notifyEat();
                    //aggiorno il box precedente del pezzo spostato
                    pieceToMove.getBox().get().setPiece(Optional.empty());
                    this.lastPieceMoved = Optional.of(pieceToMove);
                    return true;
                } else {
                    pieceToMove.getBox().get().setPiece(Optional.empty());
                    this.lastPieceMoved = Optional.of(pieceToMove);
                    return true;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Checks if pieceToMove can move in newPosition.
     * @param newPosition the Box indicating the move of pieceToMove
     * @param pieceToMove the Piece that has to be moved
     * @return true if pieceToMove can moves in newPosition, otherwise false
     */
    public abstract boolean checkMove(Box newPosition, PieceImpl pieceToMove);

    @Override
    public final void updateMove(final PieceImpl pieceToMove, final Optional<Box> newPosition) {
        if (!newPosition.isPresent()) {
            LOGGER.debug("empty optional passed as Box in the call to updateMove");
            throw new IllegalArgumentException();
        }
            pieceToMove.setBox(newPosition);
            newPosition.get().setPiece(Optional.of(pieceToMove));
            this.lastPieceMoved = Optional.of(pieceToMove);
            this.checkPromotion(pieceToMove, newPosition.get());
            this.checkWinner(pieceToMove.getColour());

    }

    /**
     * Establishes and verifies the conditions for the promotion.
     * @param pieceToMove the Piece that has to be promoted
     * @param newPosition the Box indicating the move of pieceToMove
     */
    public abstract void checkPromotion(PieceImpl pieceToMove, Box newPosition);

    /**
     * Establishes and verifies the conditions for eating piece.
     * @param newPosition the Box indicating the move of pieceToMove
     * @param pieceToMove the Piece that has to be moved in newPosition
     * @return true if pieceToMove must eat a Piece, otherwise false
     */
    public abstract boolean checkEat(Box newPosition, PieceImpl pieceToMove);

    @Override
    public final void eat(final PieceImpl hungry, final PieceImpl pieceEaten) { 
        this.lastPieceEated.add(pieceEaten);
        this.lastPieceMoved = Optional.of(hungry);
        this.getPlayer(pieceEaten.getColour()).removePiece(pieceEaten);
        this.pieceRemover(pieceEaten);
    }

    /**
     * 
     * @param pieceEaten the piece that has to be removed
     */
    public abstract void pieceRemover(PieceImpl pieceEaten);

    /* (non-Javadoc)
     * @see boardgames.model.Game#attach(boardgames.view.game.GameView)
     */
    @Override
    public void attach(final GameView gv) {
        this.observers.add(gv);
    }

    @Override
    public final void notifyEat() {
        this.observers.forEach(a -> a.sendPieceToGraveyard());
    }

    @Override
    public final void notifyPromotion() {
        this.observers.forEach(a -> a.updatePromotionStatus());
    }

    @Override
    public final void notifyConsole(final String arg) {
        this.observers.forEach(a -> a.updateConsole(arg));
    }

    @Override
    public final Player getPlayer(final Colour colourOfPlayer) {
        if (colourOfPlayer.equals(Colour.Black)) {
            return this.blackPlayer;
        } else {
            return this.whitePlayer;
        }
    }

    @Override
    public final void setPlayer(final Colour colourOfPlayer, final Player player) {
        if (!player.getColour().equals(colourOfPlayer)) {
            LOGGER.debug("Can't set the " + player.getColour().toString() + " player as the " + colourOfPlayer.toString() + " player");
            throw new IllegalStateException();
        } else if (colourOfPlayer.equals(Colour.White)) {
            this.whitePlayer = player;
        } else {
            this.blackPlayer = player;
        }
    }

    /**
     * Update the last Piece moved in the piece indicated by pieceUpgraded.
     * @param player the Colour of the Player that has to do promotion
     * @param pieceUpgraded the name of the new Piece upgraded
     */
    public abstract void promotion(Colour player, String pieceUpgraded);

    /**
     * @param player the Color of the Player on which we are controlling the winnings.
     * @return true if the indicated player is the winner, otherwise false
     */
    public abstract boolean checkWinner(Colour player);

    @Override
    public final List<PieceImpl> getLastPieceEated() {
        return this.lastPieceEated;
    }

    @Override
    public final Optional<PieceImpl> getLastPieceMoved() {
        return this.lastPieceMoved;
    }

    @Override
    public final void setLastPieceEated(final PieceImpl lastPieceEated) {
        this.lastPieceEated.add(lastPieceEated);
    }

    @Override
    public final void setLastPieceMoved(final Optional<PieceImpl> lastPieceMoved) {
        this.lastPieceMoved = lastPieceMoved;
    }

    @Override
    public final List<GameView> getObservers() {
        return this.observers;
    }
}
