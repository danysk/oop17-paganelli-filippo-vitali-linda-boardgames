package boardgames.model;

import java.util.List;
import java.util.Optional;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;

/**
 * This class implements the functions useful to determine the winner of a time game.
 */
public class CheckTimedWinnerImpl implements CheckTimedWinner {

    private final GameImpl cg;

    /**
     * Set the game to get the useful values to determine the winner of a time game.
     * @param cg the GameImpl
     */
    public CheckTimedWinnerImpl(final GameImpl cg) {
        this.cg = cg;
    }

    @Override
    public final Optional<Colour> checkPieceNumber() {
        final List<PieceImpl> whitePieces = this.cg.getPlayer(Colour.White).getPlayerPieces();
        final List<PieceImpl> blackPieces = this.cg.getPlayer(Colour.Black).getPlayerPieces();
        if (whitePieces.size() > blackPieces.size()) {
            return Optional.of(Colour.White);
        }
        if (whitePieces.size() < blackPieces.size()) {
            return Optional.of(Colour.Black);
        }
        return Optional.empty();
    }
}
