package boardgames.model;

import java.util.List;
import java.util.Optional;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;
import boardgames.view.game.GameView;

/**
 * This interface defines the common model of play.
 */
public interface Game {

    /**
     * @return initialised board of selected game.
     */
    BoardImpl getBoard();

    /**
     * Check if is possible move a piece in a new box. If newPosition isn't a valid 
     * Box throws an IllegalArgumentException.
     * @param player the Colour of the Player that has to move the piece
     * @param pieceToMove the Piece that player has to move
     * @param newPosition the Box where the Piece is moved
     * @return true if piece is moved, otherwise false
     * @throws IllegalArgumentException
     */
    boolean move(Colour player, PieceImpl pieceToMove, Optional<Box> newPosition);

    /**
     * Real move of the piece on the board. If newPosition isn't a valid 
     * Box throws an IllegalArgumentException.
     * @param pieceToMove the piece that has to move
     * @param newPosition the Box where the Piece is moved
     * @throws IllegalArgumentException
     */
    void updateMove(PieceImpl pieceToMove, Optional<Box> newPosition);

    /**
     * If the second piece(pieceEaten) is eated by the first piece(hungry), lastPieceEated is correctly setted.
     * and pieceEaten removed from the board and the available pieces of his owner player
     * @param hungry the Piece that has to eat
     * @param pieceEaten the Piece eated by hungry
     */
    void eat(PieceImpl hungry, PieceImpl pieceEaten);

    /**
     * Inserts the indicated GameView gv as an observer.
     * @param gv the observer that has to be added
     */
    void attach(GameView gv);

    /**
     * Notify all observers in case of a piece eaten.
     */
    void notifyEat();

    /**
     * Notify all observers in case of a piece promotion.
     */
    void notifyPromotion();

    /**
     * @param arg the String that has to be printed in the game console
     */
    void notifyConsole(String arg);

    /**
     * @param colourOfPlayer the Colour of the Player
     * @return the Player that has the indicated Colour
     */
    Player getPlayer(Colour colourOfPlayer);

    /**
     * Sets the indicated player to the game with the indicated colour.
     * If the player indicated has a different colour to the indicated one,
     * throws an IllegalStateException. 
     * @param colourOfPlayer the Colour of the Player
     * @param player the player that was initialized
     * @throws IllegalStateException
     */
    void setPlayer(Colour colourOfPlayer, Player player);

    /**
     * @return a List containing the Pieces eated in the last moves
     */
    List<PieceImpl> getLastPieceEated();

    /**
     * @return an Optional containing the last piece moved, otherwise and empty Optional
     */
    Optional<PieceImpl> getLastPieceMoved();

    /**
     * Sets lastPieceEated as the last Piece eated.
     * @param lastPieceEated the last Piece eated
     */
    void setLastPieceEated(PieceImpl lastPieceEated);

    /**
     * Sets lastPieceMoved as the last Piece moved.
     * @param lastPieceMoved the last Piece moved
     */
    void setLastPieceMoved(Optional<PieceImpl> lastPieceMoved);

    /**
     * @return a List containing the game's observers
     */
    List<GameView> getObservers();
}
