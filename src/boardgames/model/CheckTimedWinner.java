package boardgames.model;

import java.util.Optional;
import boardgames.utility.Colour;

/**
 * This interface defines the functions useful for determining the winner of a time game.
 */
public interface CheckTimedWinner {

    /**
     * Checks the winner of a timed match.
     * @return the Colour of the Player who has eated more Pieces, otherwise an empty Optional
     */
    Optional<Colour> checkPieceNumber();
}
