package boardgames.model;

import java.util.List;
import java.util.Optional;

import boardgames.model.board.Box;
import boardgames.model.board.CheckersBoard;
import boardgames.model.piece.Dama;
import boardgames.model.piece.CheckersPawn;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.AllCheckersPieces;
import boardgames.utility.Colour;

/**
 * This class implements the abstract functions defined by GameImpl useful for establishing the logic of Checkers.
 *
 */
public class CheckersGame extends GameImpl {

    /**
     * Constructor of the class that sets the Checkers board type and initialize the players.
     *
     */
    public CheckersGame() {
        super();
        this.getBoard().setBoardType(new CheckersBoard());
        this.getBoard().reset();
        this.setPlayer(Colour.White, new PlayerImpl(Colour.White, this.getBoard()));
        this.setPlayer(Colour.Black, new PlayerImpl(Colour.Black, this.getBoard()));
    }

    @Override
    public final void checkPromotion(final PieceImpl pieceToMove, final Box newPosition) {
        if (pieceToMove.getName().equals(AllCheckersPieces.Pawn.toString())) {
            if (pieceToMove.getColour() == Colour.Black && newPosition.getY() == LOWER_BOUND) {
                this.notifyPromotion();
            } else if (pieceToMove.getColour() == Colour.White && newPosition.getY() == UPPER_BOUND) {
                this.notifyPromotion();
            }
        }
    }

    @Override
    public final boolean checkWinner(final Colour player) {
        if (this.getPlayer(player.equals(Colour.Black) ? Colour.White : Colour.Black).getPlayerPieces().isEmpty()) {
            this.notifyVictory();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public final void promotion(final Colour player, final String pieceUpgraded) {
        if (!pieceUpgraded.equals("King")) {
            throw new IllegalArgumentException();
        }
        this.getLastPieceMoved().get().setPieceType(new Dama());
        this.checkWinner(player);
    }

    @Override
    public final boolean checkEat(final Box newPosition, final PieceImpl pieceToMove) {
       final List<PieceImpl> list;
        if (pieceToMove.getName().equals(AllCheckersPieces.Pawn.toString())) {
            final CheckersPawn piece = (CheckersPawn) pieceToMove.getPieceType().get();
            list = piece.getPossibleEated(newPosition);
        } else {
            final Dama piece = (Dama) pieceToMove.getPieceType().get();
            list = piece.getPossibleEated(newPosition);
        }
        if (list.isEmpty()) {
            return false;
        } else {
            list.forEach(a -> {
                this.eat(pieceToMove, a);
            });
            return true;
        }
    }

    @Override
    public final boolean checkMove(final Box newPosition, final PieceImpl pieceToMove) {
        return pieceToMove.possibleMoves(this.getBoard()).contains(newPosition);
    }


    @Override
    public final void pieceRemover(final PieceImpl pieceEaten) {
        if (pieceEaten.getColour().equals(Colour.Black)) {
          this.getBoard().removeBlackPiece(pieceEaten);
        } else {
          this.getBoard().removeWhitePiece(pieceEaten);
        }
        pieceEaten.getBox().get().setPiece(Optional.empty());
    }

    private void notifyVictory() {
        getObservers().forEach(o -> o.updateWinStatus());
    }
}
