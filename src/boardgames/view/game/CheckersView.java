package boardgames.view.game;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import boardgames.controller.GameControllerImpl;
import boardgames.view.board.BoardView;

/**
 * Class that manage the view "action" for Checkers game like create promotion panel, set movement
 * and positioning icon on GUI.
 *
 */
public class CheckersView extends GameViewImpl {

    private static final Logger LOGGER = LogManager.getLogger(CheckersView.class.getName());

    /**
     * @param c controller of entire game
     * @param bv checkers board view
     * @param timed true if is a timed match
     */
    public CheckersView(final GameControllerImpl c, final BoardView bv, final boolean timed) {
        super(c, bv, timed);
    }

    @Override
    public final JPanel promotionPanel(final JFrame frame) {
        final JPanel panel = getPopFactory().cratePanel(
                Optional.of(GameControllerImpl.getController().getOppositePlayerColour() + " king"),
                new GridBagLayout(), true);
        final GridBagConstraints gbc = getPopFactory().createGridBagConstraints();
        panel.add(
                getPopFactory().createButton(Optional.of(e -> this.replaceAction(frame, "King")), Optional.of("King")),
                gbc);

        return panel;
    }

    @Override
    protected void popUpImplementation() {
    }

    @Override
    public final void setMovement(final JButton b) {
        if (!getBoardView().getButtons().get(b).get().getPiece().equals(Optional.empty())) {
            GameControllerImpl.getController().setPieceToMove(getBoardView().getButtons().get(b).get().getPiece());
            this.lightUpPossibleMoves(b);
        } else if (getBoardView().getButtons().get(b).get().getPiece().equals(Optional.empty())
                && !GameControllerImpl.getController().getPieceToMove().equals(Optional.empty())) {
            GameControllerImpl.getController().movePieceTo(getBoardView().getButtons().get(b));
        }

    }

    @Override
    public final void setIconAfterMove(final JButton arrival, final JButton start) {
        arrival.setIcon(start.getIcon());
        start.setIcon(null);

        LOGGER.debug("Eated pieces " + GameControllerImpl.getController().getEatedPiece());

        getBoardView().getButtons().keySet().forEach(b -> {
            GameControllerImpl.getController().getEatedPiece().stream()
            .filter(piece -> getBoardView().getButtons().get(b).equals(piece.getBox()))
            .forEach(piece -> b.setIcon(null));
        });

    }

}
